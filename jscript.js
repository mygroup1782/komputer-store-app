const workDiv = document.getElementById('workCash')
const btnBank = document.getElementById('bankBtn')
const btnWork = document.getElementById('workBtn')
const btnLoan = document.getElementById('loanBtn')
const btnPayLoan = document.getElementById('payLoanBtn')
const btnBuyLaptop = document.getElementById('buyLaptopBtn')
const lapTops = document.getElementById('laptops')
const laptopDiv = document.getElementById('selectedLaptop')
const laptopDiv2 = document.getElementById('laptopInformation')
const pCurrentBalance = document.getElementById('currentBalance')
const pWorkCash = document.getElementById('workCash')
const pLoanBalance = document.getElementById('currentLoanBalance')
const pShowInfo = document.getElementById('showInfo')

// API for information about the laptops but also the images.
const API = 'https://noroff-komputer-store-api.herokuapp.com/computers';
const imageURL = 'https://noroff-komputer-store-api.herokuapp.com/';

// variabels that are used throughout the app.
let payment = 0;
let currentLoan = 0;
let restPay = 0;
let bankBalance = 200;
let slaptop = '';
let plaptop = 0;

// function to get 100 kr each tick when pressing the button "Work".
function getPaid() {
    payment = payment + 100;
    pWorkCash.innerText = payment + " kr";
}

// function to add the current "Work" balance towards the bank balance.
// also pays 10% interest if loan is active.
function addBankBalance() {
    if (currentLoan > 0) {
        let interest = payment * 0.1;
        payment = payment - interest;
        if (currentLoan < interest) {
            payment = payment + (interest - currentLoan);
        }
        else {
            currentLoan = currentLoan - interest;
        }
        pLoanBalance.innerHTML = currentLoan + " kr";
    }
    bankBalance = bankBalance + payment;
    payment = 0;
    pWorkCash.innerHTML = payment + " kr";
    pCurrentBalance.innerHTML = bankBalance + " kr";
}

// function for taking a loan and also updates the bank balance, 
// but also shows how much you need to pay back on your current loan.
function takeLoan() {
    const requestLoan = prompt("How much do you need?", 0);
    // an if statement that checks if you are trying to take a 
    // loan thats more then twice as big as your current bank balance.
    if (requestLoan > (bankBalance * 2)) {
        alert("You can't take a loan thats more than twice as big as your current bank balance!")
    }
    else if (requestLoan !== null) {
        bankBalance = bankBalance + parseInt(requestLoan);
        pCurrentBalance.innerHTML = bankBalance + " kr";
        currentLoan = requestLoan;
        pLoanBalance.innerHTML = "Current loan: " + currentLoan + " kr";
        btnLoan.hidden = true;
        btnPayLoan.hidden = false;
    }
}

// function for paying back the current loan.
function payBackLoan() {

    if (payment >= currentLoan) {
        restPay = payment - currentLoan;
        bankBalance = bankBalance + restPay;
        payment = 0;
        currentLoan = 0;
        btnLoan.hidden = false;
        btnPayLoan.hidden = true;
        pLoanBalance.innerHTML = "-----";
    }
    else if (payment < currentLoan) {
        restPay = currentLoan - payment;
        currentLoan = restPay;
        payment = 0;
        pLoanBalance.innerHTML = currentLoan + " kr";
    }

    pWorkCash.innerHTML = payment + " kr";
    pCurrentBalance.innerHTML = bankBalance + " kr";

}



// async function which makes an API call to get all the laptops inside a select box.
async function showLaptop() {
    const response = await fetch(API, {
        method: 'GET'
    }
    );
    if (!response.ok) {
        throw new Error(`HTTP error! status: ${response.status}`);
    }
    const data = await response.json();
    // an for-loop to loop out the objects from response.json.
    for (let i = 0; i < data.length; i++) {
        const laptopItem = document.createElement('option');
        laptopItem.appendChild(document.createTextNode(data[i].title));
        lapTops.appendChild(laptopItem);
    }
    
}

// a second async function in order to show the specs of the selected laptop.
async function selectLaptop() {
    const response = await fetch(API, {
        method: 'GET'
    });
    if (!response.ok) {
        throw new Error(`HTTP error! status: ${response.status}`);
    }
    const data = await response.json();
    let getLaptopId = document.getElementById('laptops').selectedIndex;
    
    laptopDiv.innerHTML = "";
    laptopDiv2.innerHTML = "";
    const getLaptop = document.createElement('p');
    const laptopTitle = document.createElement('h2');
    const laptopDescription = document.createElement('p');
    const laptopPrice = document.createElement('p');
    const laptopImage = document.createElement('img');
    // const format is used to get the image of each laptop.
    const format = data[getLaptopId].image;
    laptopImage.src = `${imageURL}${format}`;
    
    laptopTitle.appendChild(document.createTextNode(data[getLaptopId].title));
    laptopDescription.appendChild(document.createTextNode(data[getLaptopId].description));
    laptopPrice.appendChild(document.createTextNode(data[getLaptopId].price + " kr"));
    for (let i = 0; i < data[getLaptopId].specs.length; i++) {
        getLaptop.appendChild(document.createTextNode(data[getLaptopId].specs[i] + "\n"));
        
    }
    
    // the lines below sets everything into divs so it can be seen on the webpage.
    laptopDiv.appendChild(getLaptop);
    laptopDiv2.appendChild(laptopImage);
    laptopDiv2.appendChild(laptopTitle);
    laptopDiv2.appendChild(laptopDescription);
    laptopDiv2.appendChild(laptopPrice);
    
    btnBuyLaptop.hidden = false;
    slaptop = data[getLaptopId].title;
    plaptop = data[getLaptopId].price;
    
}

// function to buy laptops which uses title and price from API called.
function buyLaptop() {
if(bankBalance >= plaptop) {
        bankBalance = bankBalance - plaptop;
        alert(`Congrats, you are now the owner of a ${slaptop}`);
        pCurrentBalance.innerHTML = bankBalance + " kr";
    }else {
        alert(`You don't have enough money to buy a ${slaptop}`);
    }
}

// showLaptop runs when the server starts so all the laptops can be selected from the select box.
showLaptop();
btnBuyLaptop.hidden = true;
btnPayLoan.hidden = true;

btnWork.addEventListener('click', getPaid)
btnBank.addEventListener('click', addBankBalance)
btnLoan.addEventListener('click', takeLoan)
btnPayLoan.addEventListener('click', payBackLoan)
btnBuyLaptop.addEventListener('click', buyLaptop)
